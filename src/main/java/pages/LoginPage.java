package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPage {
	
	@AndroidFindBy(xpath = "//*[contains(@text, ENTRAR COM SENHA'')]")
	private MobileElement entrarComSenha;
	
	public void entrarComSenha() {
		entrarComSenha.click();
	}

}
