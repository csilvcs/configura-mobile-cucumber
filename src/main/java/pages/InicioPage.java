package pages;

import org.openqa.selenium.support.PageFactory;

import configuracoes.Capabilities;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class InicioPage extends Capabilities {

	@AndroidFindBy(accessibility = "Pr�ximo")
	private MobileElement proximo1;

	@AndroidFindBy(accessibility = "Vamos l�")
	private MobileElement vamosLa;

	@AndroidFindBy(xpath = "//*[contains(@text, 'Serasa')]")
	private MobileElement telaBoasVindas;

	@AndroidFindBy(xpath = "//android.widget.EditText[@content-desc='Digite o seu CPF']")
	private MobileElement digiteSeuCPF;

	@AndroidFindBy(accessibility = "Continuar")
	private MobileElement continuar;
	
	public void carregarTelaInicial() {
		PageFactory.initElements(new AppiumFieldDecorator(getMobileDriver()), this);
		waitElementsToBeVisible(proximo1);
	}

	public void pularApresentacao() throws InterruptedException {
		proximo1.click();
		Thread.sleep(5000);
		proximo1.click();
		Thread.sleep(5000);
		vamosLa.click();
	}

	public void digitarCpf(String cpf) throws InterruptedException {
//		waitElementsToBeVisible(digiteSeuCPF);
		Thread.sleep(5000);
		digiteSeuCPF.click();
		digiteSeuCPF.sendKeys(cpf);
	}
	
	public void continuar() {
		continuar.click();
		
	}

}
