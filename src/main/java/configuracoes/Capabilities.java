package configuracoes;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static java.time.Duration.ofSeconds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;

public class Capabilities {

	private final static int waitTimeout = 60;
	private static AppiumDriver<MobileElement> driver;
	private static WebDriverWait wait;

	public static AppiumDriver<MobileElement> getMobileDriver() {
		return driver;
	}

	public static void inicializaApp() throws Exception {
		inicializaApp(TipoAppDriver.ANDROID);
	}

	public static void inicializaApp(TipoAppDriver tipoAppDriver) throws Exception {

		switch (tipoAppDriver) {
		case ANDROID:
			
			deviceStart();

			try {
				System.out.println("Entrei no capa android");
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", getDeviceSerialNumber());
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
				capabilities.setCapability("platformVersion", getAndroidVersionDevice());
				// capabilities.setCapability("app", "C:\\caminho\\.apk");
				capabilities.setCapability("appPackage", "br.com.serasaexperian.consumidor");
//				capabilities.setCapability("appPackage", "com.conduit.app_4030e33c570d45ce8db1b96630f694e1.app");
//				capabilities.setCapability("appActivity","com.conduit.app.ConduitFragAct");

				capabilities.setCapability("appActivity","br.com.serasaexperian.consumidor.ui.view.onboarding.WelcomeActivity");
				capabilities.setCapability("automationName", "UIAutomator2");
				capabilities.setCapability("newCommandTimeout", 60000);
				capabilities.setCapability("autoGrantPermissions", true);
				capabilities.setCapability("unicodeKeyboard", true);
				capabilities.setCapability("resetKeyboard", true);

				iniciaDriver(tipoAppDriver, capabilities);

				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				wait = new WebDriverWait(getMobileDriver(), waitTimeout);

			} catch (Exception e) {
				throw new RuntimeException("Erro ao inicializar app: " + e);
			}

			break;

		case IOS:

			try {
				System.out.println("Entrei no capa IOS");
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("deviceName", "iPhone 7"); // No caso de ser via Real Device, o valor muda
																		// para iPhone SE
				// capabilities.setCapability("udid",
				// "ac2e5a61a7c7939c6603cdb40c8c6adc9a0f9b40"); //Descomentar caso Seja Device
				// Real
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
				capabilities.setCapability("platformVersion", "12.2"); // Device real = 12.4.1
				capabilities.setCapability("app", "/Users/edbusch/Downloads/AppPortal.app"); /// Us
				capabilities.setCapability("automationName", "XCUITest");

				// capabilities.setCapability("useNewWDA", true);
				capabilities.setCapability("xcodeOrgId", "2SA2447AYA");
				capabilities.setCapability("xcodeSigningId", "iPhone Developer");
				// capabilities.setCapability("bundleID", "br.com.uxxxnterprise.homolog");
				// capabilities.setCapability("xcodeConfigFile", "/Users/xxx/.xcconfig");

				// capabilities.setCapability("newCommandTimeout", 60000);
				// capabilities.setCapability("autoGrantPermissions", true);
				capabilities.setCapability("unicodeKeyboard", true);
				// capabilities.setCapability("resetKeyboard", true);
				// capabilities.setCapability("connectHardwareKeyboard", false);

				iniciaDriver(tipoAppDriver, capabilities);

				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				wait = new WebDriverWait(getMobileDriver(), waitTimeout);

			} catch (Exception e) {
				throw new RuntimeException("Erro ao inicializar app: " + e);
			}
			break;

		}
	}

	private static void deviceStart() throws Exception {
	
		String device = "C:\\Users\\787862\\AppData\\Local\\Android\\Sdk\\tools\\emulator @Pixel_2_API_25";
		Runtime.getRuntime().exec(device);
		Thread.sleep(30000);
		
	}

	public static String getDeviceSerialNumber() {
		String s = null;
		try {
			Process p = Runtime.getRuntime().exec("adb shell getprop | grep ro.boot.serialno");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			s = stdInput.readLine();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return s.trim().replace("[ro.boot.serialno]: [", "").replace("]", "");
	}

	public static String getAndroidVersionDevice() {
		String s = null;
		try {
			Process p = Runtime.getRuntime().exec("adb shell getprop | grep ro.build.version.release");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			s = stdInput.readLine();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return s.trim().replace("[ro.build.version.release]: [", "").replace("]", "");
	}

	private static void iniciaDriver(TipoAppDriver tipoDriver, DesiredCapabilities capabilities) {
		try {

			switch (tipoDriver) {
			case ANDROID:
				driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), capabilities);
				break;
			case IOS:
				driver = new IOSDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), capabilities);
				break;
			}

		} catch (Exception e) {
			throw new RuntimeException("Erro ao iniciar driver: " + e);
		}

	}

	public static void closeAppium() {

		driver.quit();
	}

	public static void waitElementToBeClickable(MobileElement element) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static WebDriverWait getWait(long waitTimeout) {
		return new WebDriverWait(getMobileDriver(), waitTimeout);
	}

	public static void waitElementsToBeVisible(MobileElement... elements) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until(ExpectedConditions.visibilityOfAllElements(elements));
	}

	public static void waitElementToBeInvisible(MobileElement element) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	public static void waitElementGetTextToBe(MobileElement element, String text) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until((ExpectedCondition<Boolean>) driver -> element.getText().equals(text));
	}

	public static boolean elementExists(String xpath) {
		return driver.findElements(By.xpath(xpath)).size() != 0;
	}

	public static boolean elementExists(WebElement element) {
		try {
			element.getTagName();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void waitInvisibilityOf(MobileElement element) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	public static void swipeDown(MobileElement elemento) {

		Dimension size = getMobileDriver().manage().window().getSize();
		TouchAction swipe = new TouchAction(driver);

		int largura = size.width / 2;
		int alturaInicio = (size.height / 5) * 3;
		int alturaFinal = 10;

		for (int i = 0; i < 100; i++) {
			try {
				if (elemento.isDisplayed()) {
					break;
				}
			} catch (Exception e) {
				swipe.press(PointOption.point(largura, alturaInicio)).waitAction(waitOptions(ofSeconds(2)))
						.moveTo(PointOption.point(largura, alturaFinal)).release();
				swipe.perform();
			}
		}
	}

	public static void swipeDown() {

		try {

			Dimension size = getMobileDriver().manage().window().getSize();
			TouchAction swipe = new TouchAction(driver);

			int largura = size.width / 2;
			int alturaInicio = (size.height / 5) * 3;
			int alturaFinal = 3;

			swipe.press(PointOption.point(largura, alturaInicio)).waitAction(waitOptions(ofSeconds(2)))
					.moveTo(PointOption.point(largura, alturaFinal)).release().perform();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void waitAllElementToDisplay(String xpath, long timeOutInSeconds) {
		wait = new WebDriverWait(getMobileDriver(), waitTimeout);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
	}

	public void click(MobileElement element) {
		waitElementsToBeVisible(element);
		waitElementToBeClickable(element);
		element.click();
	}

	public void sendKeys(MobileElement element, String text) {
		waitElementsToBeVisible(element);
		waitElementToBeClickable(element);
		element.sendKeys(text);
	}

	public String getText(MobileElement element) {
		waitElementsToBeVisible(element);
		return element.getText();
	}

	public boolean isDisplayed(MobileElement element) {
		return element.isDisplayed();
	}

	public boolean isSelected(MobileElement element) {
		return element.isSelected();
	}

	public boolean isEnabled(MobileElement element) {
		return element.isEnabled();
	}

	public Integer size(List<MobileElement> element) {
		waitElementsToBeVisible(element.get(0));
		return element.size();
	}

	public Dimension getSize(MobileElement element) {
		return element.getSize();
	}

	public String getTitle() {
		return getMobileDriver().getTitle();
	}
}
