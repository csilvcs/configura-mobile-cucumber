package steps;
import org.junit.AfterClass;

import configuracoes.AppiumService;
import configuracoes.Capabilities;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class HooksAutomation {

	AppiumService Appium = new AppiumService();

	@Before
	public void beforeMobile() throws Exception {
		Appium.startServer();
		Capabilities.inicializaApp();
	}

	@After
	public void afterMobile() {
		Capabilities.closeAppium();
	}

	@AfterClass
	public void encerraAppium() {
		Appium.stopServer();
	}
}
