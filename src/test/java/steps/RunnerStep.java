package steps;

import configuracoes.Capabilities;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.InicioPage;

public class RunnerStep extends Capabilities {

	InicioPage inicio = new InicioPage();

	@Given("Que eu esteja na tela inicial do aplicativo")
	public void que_eu_esteja_logado_no_aplicativo() throws InterruptedException {
		inicio.carregarTelaInicial();
		System.out.println("Entrei no app");

	}

	@When("passar a tela de boas vindas")
	public void passar_a_tela_de_boas_vindas() throws InterruptedException {
		inicio.pularApresentacao();
		System.out.println("Pulei as apresentações");
	}

	@Then("o usuario devera digitar o cpf {string}")
	public void o_usuario_devera_digitar_o_cpf(String cpf) throws InterruptedException {
		inicio.digitarCpf(cpf);
		System.out.println("Entrei na tela do login");
	}

	@Then("clicar em continuar")
	public void clicar_em_continuar() {
		inicio.continuar();
		System.out.println("Cliquei no continuar");
	}

	@Then("a tela inicial devera ser apresentada")
	public void a_tela_inicial_devera_ser_apresentada() {
		
		System.out.println("Entrei na tela inicial");
		
	}

}
